# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## 0.6.3 (2022-04-12) ##

### Fixed
* Ensure that `zdot()` does not produce NaNs when the input data are constant


## 0.6.2 (2022-02-24) ##

### Added
* Option to time scrunch the plots in candreport (`--tscrunch <FACTOR>`)


## 0.6.1 (2021-12-09) ##

This update makes available additional functions that allow to conveniently reproduce the new RFI mitigation process inside the real-time pipeline.

### Fixed
* Add `spyden` as a required module, it had always been a dependency of the `mtcutils-candreport` app (and of its ancestor), since it provides the convolution and S/N calculation kernels.

### Added

* `mtcutils.core` is now a submodule rather than just a file `core.py` in the base directory of the module. This is about separating core functions clearly, and this won't break existing dependent code.
* Added new core functions to conveniently reproduce the new real-time RFI mitigation process. In particular:
    * `automatic_channel_mask`: Infers the channel mask from the data and returns it as a boolean array
    * `automatic_clean`: Applies the whole RFI mitigation process out of place and returns a clean copy of the input data
* The `mtcutils-candreport` app now has a `-v, --version` command-line argument that returns the current module version.
* New `mtcutils-snapshot` app to visually check the readability of what the `snapshot()` function produces
* Updated `README`
* In Travis CI config, upgrade `pip` before install to reduce chance of dependency conflicts.
* Add python 3.9 to Travis CI builds.

### Changed

* Removed optional argument `mask` from `snapshot` function, which now just applies the automatic masking procedure via `automatic_channel_mask`


## 0.6.0 (2021-11-09) ##

This update follows changes made to the RFI mitigation inside the MeerTRAP real-time pipeline, that we now match here.

### Added
* `Candidate` now has `zdot()` method that applies the Z-dot filter in place
* Option to use interactive backend in `mtcutils-candreport`

### Changed
* When given no argument, `Candidate.apply_chanmask()` method now masks all channels whose stddev is abnormal (as determined by IQRM), but also the channels whose acc1 (autocorrelation coefficient with a lag of 1 time sample) is abnormal.
* The `snapshot()` function now returns a version of the data where the full RFI cleaning procedure of the real-time pipeline has been applied. This includes masking channels based on acc1, applying the zdot filter, and clipping the data to [-3, +3] sigma before scrunching it for the final output. The output data scale is now set with a more sensible method.
* Replace the zero-DM filter by the Z-dot filter in `mtcutils-candreport`. It can be disabled by the `--no-zdot` flag. The `--no-zerodm` flag has been removed.


## 0.5.1 (2021-07-19) ##
### Added
* Candidate analysis and diagnostic plot app `mtcutils-candreport`. The app loads a candidate, cleans it, computes the its dedispersion transform across a user-defined DM range, convolves the dedispersed time series with Gaussian matched filters, and returns the best S/N. The process is as follows:  
    - Load the data
    - Apply IQRM
    - Estimate the standard deviation of the noise from the raw data
    - Apply a zero-DM filter (can be disabled). The zero-DM time series is calculated over the unmasked channels, and subtracted only from those.
    - Calculate the dedispersion transform (DDTR) of the data over the specified DM range. **The exact DM trial values are fixed:** the DM step corresponds to a top to bottom dispersion delay of one sample, and all DM trials are an integer multiple of this value. The DDTR is calculated using brute-force (exact) dedispersion.
    - Convolve the dedispersed time series with Gaussian matched filters. The filter widths range from 1 to 200 samples, and are geometrically spaced with a factor of 1.25.
    - Report best S/N and save plots
* Added `times` property to `Candidate` class (time offsets of each sample in seconds)


## 0.5.0 (2021-07-15) ##

### Added
* Unit tests with `pytest`
* Automatic versioning with `versioneer` (using versioneer 0.18 for compatibility with Python 2.7)
* Travis CI

### Changed
* Removed all IQRM functions, use the official `iqrm` module instead.


## 0.4.1 (2020-06-04) ##

### Added
* Method `zerodm()` that applies the zero-DM filter in place to a `Candidate` object


## 0.4.0 (2020-05-04) ##

This version re-organises the module in view of being used for pre-processing candidates before being evaluated by FETCH.

### Changed
* Moved channel masking functions (formerly in `bandpass` sub-module) to a single file `iqrm.py`. The `shdiff_mask()` function is now called `iqrm_mask()` and is directly available in the root `mtcutils` module.
* In IQRM outlier flagging algorithm, flag only channels whose standard deviation is more than `nsigma` **above** expectation. This is the theoretically sensible thing to do. Formerly, any channel whose standard deviation was more than `nsigma` **below** expectation was also masked as a workaround for some edge cases. We definitely do not want to do this anymore in FETCH pre-processing since over-masking can potentially disrupt the FETCH prediction, and we will be clipping the data anyway.


### Added
* Candidate snapshot function for the TUSE control system
* Generic `scrunch()` and `normalise()` functions that operate on a 2D numpy array directly

### Fixed
* Do nothing instead of raising an error when calling `Candidate.normalise()` multiple times


## 0.3.0 (2020-03-06) ##
**This version fixes a major bug, see below.**

### Fixed
* SIGPROC filterbank data is now correctly read as 8-bit *unsigned* integers. This was causing strange negative pulse issues.

### Added
* Added `to_maskstring()` function, converting a boolean mask into a maskstring usable by the TUSE control system


## 0.2.1 (2020-01-29) ##
### Added
* Added `parse_maskstring()` function to create a boolean channel mask from a string specifier used in the TUSE control system

## 0.2.0 (2019-11-19) ##
### Changed
* Data order in `Candidate` has been changed to FT (frequency, time), which is more natural to manipulate for the user

### Fixed
* Fixed `Candidate.scrunched_data()` which was returning wrong output

### Changed
* Renamed `Candidate.load_filterbank()` to `from_filterbank()`

### Removed
* Removed the fast dm transform sub-module `mtcutils.fdmt` which will be moved to a separate package


## 0.1.0 (2019-11-07) ##
### Added
* `Candidate` class now has a `load_filterbank` classmethod, which is currently equivalent to `__init__`
* Added app `bpfetch.py` to collect the bandpass standard deviation of large batches of candidates, using multiprocessing. Defined a console entry point `bpfetch` for the app.
* Added working fast DM transform implementation in `mtcutils.fdmt`, to be improved

### Changed
* Default `maxlag` parameter for bandpass masking function `shdiff_mask` is now 3 (down from 5)

### 0.0.0 (2019-09-27) ###
### Added
* Initial commit
# mtcutils

[![Build Status](https://travis-ci.com/vmorello/mtcutils.svg?branch=master)](https://travis-ci.com/vmorello/mtcutils)

Utilities for reading, manipulating and post-processing MeerTRAP Candidate files.

### Installation

Clone the repository and install in editable mode:  
```
git clone https://vmorello@bitbucket.org/vmorello/mtcutils.git
make install
```

### Core data manipulation functions

A number of useful data manipulation functions are made available under the `mtcutils.core` sub-module. Almost all of them operate on two-dimensional arrays that represent a block of search-mode data in frequency-time (FT) order, i.e. time is contiguous in memory.

The current list is:  
* `automatic_channel_mask`
* `automatic_clean`
* `normalise`
* `scrunch`
* `spectral_acc1`
* `tukey_mask`
* `tukey_mask_lowtailed`
* `zdot`

The names and docstrings of these functions should contain all the necessary information to use them well.

### Apps

Installing the module also installs the `mtcutils-candreport` app as a console script. This can be used to throughly assess the quality of a single pulse candidate. The app loads a candidate, applies an RFI mitigation procedure that closely matches that of the real-time pipeline, runs a high quality single pulse search procedure and finally returns the best detection parameters with high-resolution candidate plots. See the app's help to get started:  

```
mtcutils-candreport -h
```

Another console script is `mtcutils-snapshot`, which allows to check snapshots of candidate files (for test purposes mostly).

### Candidate class

Candidate files can be conveniently loaded an manipulated via the `Candidate` class. Here's a quickstart example:

```python
from mtcutils import Candidate
import matplotlib.pyplot as plt

# Load a MeerTRAP candidate file
c = Candidate.from_filterbank("some_candidate.fil")

# Normalise channels to zero mean and unit variance, in place
c.normalise()

# Apply a boolean channel mask to the data, in place.
# If no mask is specified, this function will instead infer a mask from
# the data directly, following what is currently done inside the MeerTRAP
# pipeline
mask = c.apply_chanmask()

# Set the data dispersion measure, in place. This operation is reversible.
# NOTE: the channels are circularly shifted in place, meaning that the
# dispersion trails wrap around. The data near the end of the block will
# therefore be incorrect for any DM > 0
c.set_dm(256.0)

# Plot of a scrunched copy of the data (in frequency-time order)
X = c.scrunched_data(t=8, f=16)
plt.imshow(X, cmap='Greys')

# Get dedispersed time series at DM = 1999.0, summing along the frequency axis
c.set_dm(1999.0)
ts = c.data.sum(axis=0)
```
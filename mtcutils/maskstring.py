#
# Functions to read channel mask string specifiers
# in the format used by the control system
#
import itertools
import numpy as np

def maskstring_to_slices(specifier, nchan):
    """
    Parse a string that specifies a list of int ranges, with the goal of 
    creating a channel mask.

    Parameters
    ----------
    specifier: str
        Comma separated list of either channel indices or ranges of indices
        given as 'start:end', where start is INclusive and end EXclusive,
        following the python convention. Spaces do not matter.
        Valid examples:
        "" : no indices selected
        "42": index 42 only
        "10, 200:300": index 10 and indices 200 to 299 inclusive
    nchan: int
        The number of channels of the intended channel mask. This parameter
        is used to check if the ranges specified are within [0, nchan-1].

    Returns
    -------
    slices: list
        A list of slice objects. If 'specifier' is an empty string, then this
        will be an empty list. Each element of the list specifies a range of 
        channel indices to mask.
    """
    def parse(spec):
        try:
            n = int(spec)
            return slice(n, n+1)
        except ValueError:
            pass

        try:
            start, stop = spec.split(':')
            start = int(start)
            stop = int(stop)
            return slice(start, stop)
        except ValueError:
            raise ValueError("Invalid range specifier: {!r}".format(spec))

    def parse_check(spec):
        sl = parse(spec)
        if sl.start > sl.stop:
            raise ValueError("{!r} specifies a range with start > stop".format(spec))

        if sl.start < 0 or sl.stop > nchan:
            raise ValueError("{!r} specifies a channel index out range (nchan={})".format(spec, nchan))
        return sl

    # Empty specifier case
    if not specifier:
        return []

    return [parse_check(s) for s in specifier.split(',')]


def parse_maskstring(maskstring, nchan):
    """
    Create a boolean channel mask from a multi-range specifier.

    Parameters
    ----------
    maskstring: str
        String that specifies a list of channel ranges, see maskstring_to_slices()
    nchan: int
        The number of channels of the intended channel mask. This parameter
        is used to check if the ranges specified are within [0, nchan-1]

    Returns
    -------
    chanmask: ndarray
        Boolean array with nchan elements, where True denotes a bad channel
    """
    slices = maskstring_to_slices(maskstring, nchan=nchan)
    chanmask = nchan * [0]
    for sl in slices:
        chanmask[sl] = [1] * abs(sl.stop - sl.start)
    return np.asarray(chanmask, dtype=bool)


def to_maskstring(mask):
    """
    Convert boolean mask to maskstring

    Parameters
    ----------
    chanmask: ndarray
        Boolean array with nchan elements, where True denotes a bad channel

    Returns
    -------
    maskstring: str
        Multi-range specifier. See parse_multirange_specifier()
    """
    slices = []
    for flag, indices in itertools.groupby(range(mask.size), lambda n: mask[n]):
        if flag:
            # 'indices' is a list of contiguous indices where mask is True
            indices = list(indices)
            sl = slice(indices[0], indices[-1] + 1)
            slices.append(sl)

    def slicestring(sl):
        if sl.stop - sl.start == 1:
            return str(sl.start)
        else:
            return "{}:{}".format(sl.start, sl.stop)

    return ','.join([slicestring(sl) for sl in slices])


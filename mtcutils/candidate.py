import os
import numpy as np
from iqrm import iqrm_mask

from mtcutils.reading import SigprocHeader
from mtcutils.dedisp import dispersion_shifts, roll2d
from mtcutils.maskstring import parse_maskstring
from mtcutils.core import scrunch, zdot, spectral_acc1, tukey_mask, tukey_mask_lowtailed

class Candidate(object):
    """ 
    MeerTRAP Candidate class.
    
    Parameters
    ----------
    fname: str
        Path to file
    """
    def __init__(self, fname):
        self._fname = os.path.realpath(fname)
        self._header = SigprocHeader(fname)

        # data in TF order, as a float32 numpy 2D array
        self._data = self._load_data()

        # Calculate original channel standard deviations, 
        # before normalise() is called
        # NOTE: cast to np.float32 matters, otherwise normalising the data
        # will automatically upcast it to float64
        self._bpmean = self._data.mean(axis=1, dtype=float).astype(np.float32)
        self._bpstd = self._data.std(axis=1, dtype=float).astype(np.float32)
        self._bpacc1 = spectral_acc1(self._data)
        self._normalised = False

        # Track current DM and channel shifts so the dedispersion
        # operation can be reversed
        self._dm = 0.0
        self._dispersion_shifts = np.zeros(self.nchans, dtype=int)

    def _load_data(self):
        """ Load data and place it in FT order """
        with open(self._fname, 'rb') as fobj:
            fobj.seek(self._header.bytesize)
            data = np.fromfile(fobj, dtype=np.uint8).reshape(-1, self._header['nchans'])
        # Transpose to FT order, copy() ensures that the data 
        # are in C order in memory
        data = data.T.copy()
        return data.astype(np.float32)

    @classmethod
    def from_filterbank(cls, fname):
        return cls(fname)

    @property
    def data(self):
        """ Data in frequency-time order """
        return self._data

    @property
    def nsamp(self):
        """ Number of samples in the data """
        return self.data.shape[1]

    @property
    def nchans(self):
        """ Number of channels in the data """
        return self._header['nchans']

    @property
    def fch1(self):
        """ Frequency of the first channel (MHz) """
        return self._header['fch1']

    @property
    def foff(self):
        """ Frequency offset between consecutive channels (MHz). Can be negative. """
        return self._header['foff']

    @property
    def fchn(self):
        """ Frequency of the last channel (MHz) """
        return self.fch1 + self.foff * (self.nchans - 1)

    @property
    def tsamp(self):
        """ Sampling time in seconds """
        return self._header['tsamp']

    @property
    def freqs(self):
        """ 
        Channel frequencies in MHz, in the same order as they appear in the
        data 
        """
        return self.fch1 + np.arange(self.nchans) * self.foff

    @property
    def times(self):
        """
        Time offset of all samples from the start of the data, in seconds
        """
        return np.arange(self.nsamp) * self.tsamp

    @property
    def normalised(self):
        return self._normalised

    @property
    def bandpass_mean(self):
        """ Mean of the data along the time axis, BEFORE normalisation """
        return self._bpmean

    @property
    def bandpass_std(self):
        """ Standard deviation of the data along the time axis, BEFORE normalisation """
        return self._bpstd

    @property
    def bandpass_acc1(self):
        """ Autocorrelation coefficient with a lag of 1 time sample along the time axis """
        return self._bpacc1

    @property
    def dm(self):
        """ Current dispersion measure of the data in pc cm^{-3} """
        return self._dm

    def normalise(self):
        """
        Normalize all channels to zero mean and unit variance, in place. An exception is constant 
        channels, which are just set to zero.
        """
        if self._normalised:
            return

        m = self.bandpass_mean
        s = self.bandpass_std.copy() # leave original bandpass_std untouched
        s[s == 0] = 1.0
        self._data = (self._data - m.reshape(-1, 1)) / s.reshape(-1, 1)
        self._normalised = True

    def set_dm(self, dm):
        """
        Set the DM of the data to specified value, by circularly shifting the
        channels appropriately.
        """
        tsamp = self._header['tsamp']
        new_shifts = dispersion_shifts(self.freqs, dm, tsamp)
        delta_shifts = new_shifts - self._dispersion_shifts
        self._data = roll2d(self._data, -delta_shifts)
        self._dispersion_shifts = new_shifts
        self._dm = dm

    def zerodm(self):
        """
        Apply the zero-DM filter, that is subtract the mean of the data along
        the frequency axis from every frequency channel. This method takes
        care of setting the DM of the data to zero before this operation,
        and then sets it back to the original DM afterwards.

        Raises
        ------
        ValueError : if the data are not normalised yet
        """
        if not self._normalised:
            raise ValueError("Refusing to apply zero-DM filter on non-normalised data, call normalise() first")
        
        dm = self.dm
        self.set_dm(0.0)
        self._data -= self._data.mean(axis=0, dtype=float)
        self.set_dm(dm)

    def zdot(self):
        """
        Apply the Z-dot filter to the data, in place. This is a much better
        variant of the zero-DM filter.
        See Men et al. 2019:
        https://ui.adsabs.harvard.edu/abs/2019MNRAS.488.3957M/abstract

        Raises
        ------
        ValueError : if the data are not normalised yet
        """
        if not self._normalised:
            raise ValueError("Refusing to apply zero-DM filter on non-normalised data, call normalise() first")

        dm = self.dm
        self.set_dm(0.0)
        self._data = zdot(self._data)
        self.set_dm(dm)

    def apply_chanmask(self, mask=None):
        """
        Apply given channel mask, where 'True' elements represent a bad channel. 
        If no mask is specified, this function will instead infer a mask from
        the data directly, following what is currently done inside the MeerTRAP
        pipeline (as of late Oct. 2021):

        1. Run IQRM on the spectral standard deviation of the data, with
           radius = 0.1 x nchan, threshold = 3.0
        2. Find channels with abnormal ACC1 (autocorrelation coefficient with a
           lag of 1 sample). 
        
        A channel that registers as an outlier in either step is replaced by zeros.
        The function returns the boolean channel mask that was applied.

        Parameters
        ----------
        mask: ndarray or None
            Behaviour of the function depends on argument type:
            - boolean ndarray: mask channels for which the mask is True
            - None: infer using IQRM + ACC1 outlier masking

        Returns
        -------
        mask_array: ndarray
            The boolean mask that was applied
        """
        if not self.normalised:
            raise ValueError("Must normalise data first")

        if mask is None:
            radius = max(2, int(0.1 * self.nchans))
            miqrm, __ = iqrm_mask(self.bandpass_std, radius=radius, threshold=3.0)
            # NOTE: Using |acc1| would work better, but currently (Nov 2021)
            # the MeerTRAP pipeline uses acc1 without the absolute value
            # NOTE 2: Using percentiles below 50-th to determine robust stddev
            # would be better, because there are often cases where more than
            # 25% of the channels have excessive |acc1|. But here again we 
            # follow what is currently inside the pipeline.
            macc1 = tukey_mask(self.bandpass_acc1, threshold=2.0)
            mask = miqrm | macc1
        elif isinstance(mask, np.ndarray):
            pass
        else:
            raise ValueError("mask should be a boolean array or None")
        
        self._data[mask] = 0.0
        return mask

    def scrunched_data(self, t=1, f=1, select='left'):
        """
        Returns a copy of the data scrunched in time and frequency

        Parameters
        ----------
        t : int
            Time scrunching factor. If 't' does not divide 'nsamp',
            then some samples will be excluded from the scrunched data.
            Which samples are excluded depend on the 'select' argument.
        f : int
            Freq scrunching factor. If 'f' does not divide 'nchan',
            an error is raised.
        select : str
            Time sample selection policy, either 'left' or 'centre'. The number
            of samples selected from scrunching is Ns = nsamp - nsamp % t
            If 'left', select the first Ns samples.
            If 'centre', select the middle Ns samples.

        Returns
        -------
        scrunched : ndarray
            The scrunched data
        """
        return scrunch(self.data, t=t, f=f, select=select)


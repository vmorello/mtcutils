import os
import numpy as np
from pytest import raises
from mtcutils import Candidate, snapshot


CANDFILE = os.path.join(
    os.path.dirname(__file__), 
    'fake_candidate.fil'
)

EXPECTED_HEADER = {
    "fch1": 1712.0,
    "foff": -13.375,
    "nbits": 8,
    "nchans": 64,
    "nifs": 1,
    "tsamp": 0.0003062429906542,
    "tstart": 60000.0
}

def expected_data():
    """ Expected data array, in FT order, with float32 dtype """
    X = np.zeros((64, 128), dtype='float32')
    X[:, 63] = 127
    X[:, 64] = 255
    X[:, 65] = 127
    X[0] = 0
    return X

EXPECTED_DATA = expected_data()


def test_candidate_reading():
    cand = Candidate.from_filterbank(CANDFILE)

    assert cand._header == EXPECTED_HEADER
    assert cand.nsamp == 128
    assert cand.nchans == 64
    assert cand.fch1 == EXPECTED_HEADER['fch1']
    assert cand.foff == EXPECTED_HEADER['foff']
    assert cand.fchn == cand.fch1 + (cand.nchans - 1) * cand.foff
    assert cand.tsamp == EXPECTED_HEADER['tsamp']
    assert np.allclose(cand.freqs, np.arange(cand.nchans) * cand.foff + cand.fch1)
    assert np.allclose(cand.times, np.arange(cand.nsamp) * cand.tsamp)

    X = cand.data
    assert X.dtype == EXPECTED_DATA.dtype
    assert X.shape == EXPECTED_DATA.shape
    assert np.allclose(X, EXPECTED_DATA)


def test_normalise():
    cand = Candidate.from_filterbank(CANDFILE)
    cand.normalise()
    assert cand.normalised
    X = cand.data
    m = X.mean(axis=1)
    s = X.std(axis=1)

    # All channels should have zero mean
    assert np.allclose(m, 0.0, atol=np.finfo(np.float32).eps)

    # All channels should have unit stddev, except constant channels who should have
    # zero stddev
    assert np.allclose(s[1:], 1.0, atol=np.finfo(np.float32).eps)
    assert s[0] == 0


def test_apply_chanmask_manual():
    cand = Candidate.from_filterbank(CANDFILE)
    with raises(ValueError): # must normalise first
        cand.apply_chanmask()

    # Manual masking
    cand.normalise()
    mask = np.zeros(cand.nchans, dtype=bool)
    mask[0] = True
    out = cand.apply_chanmask(mask)
    assert np.alltrue(out == mask)
    assert np.allclose(cand.data[0], 0.0)


def test_apply_chanmask_auto():
    cand = Candidate.from_filterbank(CANDFILE)
    cand.normalise()
    mask = cand.apply_chanmask()
    # On our specific test data, we expect that
    # - IQRM masks nothing
    # - ACC1 masking finds that the constant channel is an outlier
    assert mask[0] == True
    assert np.alltrue(mask[1:] == False)


def test_set_dm():
    # Check that the operation is reversible as advertised:
    cand = Candidate.from_filterbank(CANDFILE)
    X = cand.data.copy()

    assert cand.dm == 0
    cand.set_dm(999.0)
    assert cand.dm == 999.0
    cand.set_dm(0.0)
    assert cand.dm == 0.0
    Y = cand.data.copy()
    assert np.allclose(X, Y)


def test_zerodm():
    cand = Candidate.from_filterbank(CANDFILE)
    with raises(ValueError): # must normalise first
        cand.zerodm()

    # The function is expected to do the right thing even if the current data DM is not 0
    # (set DM to zero, subtract zero-DM average, set DM back to original value)
    cand.set_dm(999.0)
    cand.normalise()
    cand.zerodm()

    # The new zero-DM time series should be uniformly zero
    cand.set_dm(0.0)
    assert np.allclose(cand.data.sum(axis=0), 0.0, atol=1e-4)


def test_zdot():
    cand = Candidate.from_filterbank(CANDFILE)
    with raises(ValueError): # must normalise first
        cand.zdot()

    # The function is expected to do the right thing even if the current data DM is not 0
    # (set DM to zero, subtract zero-DM average, set DM back to original value)
    cand.set_dm(999.0)
    cand.normalise()
    cand.zdot()

    # For our specific test data, the clean data should be uniformly zero
    assert np.allclose(cand.data, 0.0, atol=1e-4)


def test_zdot_allzeros():
    """
    Check that no arithmetic issues occur when applying the zdot filter
    to uniformly zero data
    """
    cand = Candidate.from_filterbank(CANDFILE)
    cand._data[:] = 0
    cand.normalise()
    cand.zdot()
    assert np.sum(np.isnan(cand.data.ravel())) == 0


def test_scrunch_data():
    cand = Candidate.from_filterbank(CANDFILE)
    X = cand.data

    # Freq scrunch
    f = 32
    S = cand.scrunched_data(t=1, f=f)
    assert S.shape == (cand.nchans // f, cand.nsamp)
    assert np.allclose(S[:,63], [127 * f - 127, 127 * f])
    assert np.allclose(S[:,64], [255 * f - 255, 255 * f])
    assert np.allclose(S[:,65], [127 * f - 127, 127 * f])

    # Time scrunch
    t = 2
    S = cand.scrunched_data(t=t, f=1)
    assert S.shape == (cand.nchans, cand.nsamp // t)
    assert np.allclose(S[0], 0)
    assert np.allclose(S[1:,31], 127)
    assert np.allclose(S[1:,32], 255 + 127)

    # Both
    S = cand.scrunched_data(t=t, f=f)
    assert S.shape == (cand.nchans // f, cand.nsamp // t)
    assert S[0, 31] == 127 * f - 127
    assert S[0, 32] == (255 + 127) * f - (255 + 127)
    assert np.allclose(S[1:,31], 127 * f)
    assert np.allclose(S[1:,32], (255 + 127) * f)


def test_snapshot():
    img = snapshot(CANDFILE, width=128, height=64)
    assert img.shape == (64, 128)

    # For our specific test data, RFI cleaning procedure inside snapshot(), and considering 
    # how the scrunched snapshot data are normalised before being converted to uint8, 
    # the output should be uniformly equal to 127
    assert np.allclose(img, 127)


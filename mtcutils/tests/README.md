`fake_candidate.fil` contains unsigned 8-bit data with 128 samples and 64 channels. The data are equal to zero, except for:  
* Sample index 64 is equal to 255 (all channels)
* Sample indices 63 and 65 are equal to 127 (all channels)
* Channel index 0 is uniformly zero (that's handy to test edge cases when normalising the data)

The content of the SIGPROC header is as follows:
```json
{
    "fch1": 1712.0,
    "foff": -13.375,
    "nbits": 8,
    "nchans": 64,
    "nifs": 1,
    "tsamp": 0.0003062429906542,
    "tstart": 60000.0
}
```
import os
import numpy as np
from pytest import raises
from mtcutils import parse_maskstring, to_maskstring


def test_maskstring():
    nchan = 10
    s = '0,1:2,2:4,5:10'
    expected_mask = np.ones(nchan, dtype=bool)
    expected_mask[4] = False
    
    mask = parse_maskstring(s, nchan)
    assert np.alltrue(mask == expected_mask)
    assert to_maskstring(parse_maskstring(s, nchan)) == '0:4,5:10'

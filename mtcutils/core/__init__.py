# NOTE: the post-processing code depends on these functions
# being importable from `mtcutils.core`
from .core import (
    automatic_channel_mask,
    automatic_clean,
    normalise,
    scrunch,
    spectral_acc1,
    tukey_mask,
    tukey_mask_lowtailed,
    zdot
)
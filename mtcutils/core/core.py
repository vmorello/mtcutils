import numpy as np

from iqrm import iqrm_mask


def scrunch(data, t=1, f=1, select='centre'):
    """
    Scrunch FT-ordered data in time and frequency, by *summing* consecutive
    samples and/or channels together. Data are always cast to float32 before
    scrunching.

    Parameters
    ----------
    data : ndarray
        2D array with the data in FT order, with shape (nchan, nsamp)
    t : int
        Time scrunching factor. If 't' does not divide 'nsamp',
        then some samples will be excluded from the scrunched data.
        Which samples are excluded depend on the 'select' argument.
    f : int
        Freq scrunching factor. If 'f' does not divide 'nchan',
        an error is raised.
    select : str
        Time sample selection policy, either 'left' or 'centre'. The number
        of samples selected from scrunching is Ns = nsamp - nsamp % t
        If 'left', select the first Ns samples.
        If 'centre', select the middle Ns samples.

    Returns
    -------
    scrunched : ndarray
        The scrunched data
    """
    if not ((t > 0) and (f > 0)):
        raise ValueError("Both scrunch factors must be > 0")
    nchans, nsamp = data.shape
    if nchans % f:
        raise ValueError("Frequency scrunch factor must divide number of channels")

    # Number of samples eligible for time scrunching
    nvalid = t * (nsamp // t)

    if select == 'left':
        X = data[:, :nvalid]
    elif select == 'centre':
        offset = (nsamp - nvalid) // 2
        X = data[:, offset:offset+nvalid]
    else:
        raise ValueError("Invalid time sample selection policy")
    
    X = X.astype(np.float32)
    X = X.reshape(nchans // f, f, nvalid // t, t).sum(axis=(1,3))
    return X


def normalise(data, clip_nsigma=None):
    """
    Normalise FT-ordered data on a per-channel basis, to zero mean and unit
    standard deviation. An exception is constant channels, which are just set
    to zero. If clip_nsigma is not None, the data are clipped after they
    have been normalised.

    Parameters
    ----------
    data : ndarray
        2D array with the data in FT order, with shape (nchan, nsamp)
    clip_nsigma : float or None
        Clip the normalised data to a range [-clip_nsigma, +clip_nsigma].
        If None, no clipping is performed.

    Returns
    -------
    Z : ndarray
        Normalised data in FT order ('float32' data type)
    m : ndarray
        Original channel means ('float32' data type)
    s : ndarray
        Original channel standard deviations ('float32' data type)
    """
    m = data.mean(axis=1, dtype=float).astype(np.float32)
    s = data.std(axis=1, dtype=float).astype(np.float32)
    s[s == 0] = 1.0  # Safety against constant channels 
    Z = (data - m.reshape(-1, 1)) / s.reshape(-1, 1)
    if clip_nsigma is not None:
        vmax = abs(clip_nsigma)
        Z = np.clip(Z, -vmax, +vmax)
    return Z, m, s


def zdot(data):
    """
    Apply the Z-dot filter to the data out of place, return a cleaned copy. 
    The Z-dot filter is a much better variant of the zero-DM filter.
    See Men et al. 2019:
    https://ui.adsabs.harvard.edu/abs/2019MNRAS.488.3957M/abstract

    Parameters
    ----------
    data : ndarray
        2D array with the data in FT order, with shape (nchan, nsamp)

    Returns
    -------
    clean_data : ndarray
        Clean data with the same shape as input
    """
    z = data.mean(axis=0)

    # Center the vector z and scale to unit L2-norm
    mu_z = z.mean()
    norm_z = np.sum((z - mu_z) ** 2) ** 0.5
    norm_z = norm_z if norm_z > 0 else 1.0  # check against constant z
    z = (z - mu_z) / norm_z

    # Subtract for each channel its vector component parallel to z
    w = (data * z).sum(axis=1)
    clean_data = data - w.reshape(-1, 1) * z
    return clean_data


def tukey_mask(x, threshold=3.0):
    """
    Given an array of values, returns a binary mask where 'True' corresponds
    to an outlier according to Tukey's rule (where the robust standard
    deviation of the data are estimated from the first and third quartiles)

    Parameters
    ----------
    x : ndarray
        Input data
    threshold : float
        Threshold in number of Gaussian sigmas

    Returns
    -------
    mask : ndarray
        Output binary mask
    """
    q1, q2, q3 = np.percentile(x, (25, 50, 75))
    stddev = (q3 - q1) / 1.349
    return abs(x - q2) > threshold * stddev


def tukey_mask_lowtailed(x, threshold=3.0):
    """
    Given an array of values, returns a binary mask where 'True' corresponds
    to an outlier according to a modified version Tukey's rule where the
    robust standard deviation of the data is determined using the first and
    second quartiles only.

    Parameters
    ----------
    x : ndarray
        Input data
    threshold : float
        Threshold in number of Gaussian sigmas

    Returns
    -------
    mask : ndarray
        Output binary mask
    """
    q1, q2 = np.percentile(x, (25, 50))
    stddev = (q2 - q1) / 0.675
    return abs(x - q2) > threshold * stddev


def spectral_acc1(data):
    """
    Returns the spectral autocorrelation coefficient with a lag of 1 sample (acc1)
    of the data.

    Parameters
    ----------
    data : ndarray
        2D array with the data in FT order, with shape (nchan, nsamp)

    Returns
    -------
    acc1 : ndarray
        Spectral autocorrelation coefficient with a lag of 1 sample, as a
        1-dimensional array with nchan elements
    """
    __, nsamp = data.shape
    M = data.mean(axis=1)
    V = data.var(axis=1)
    V[V == 0] = 1.0
    X = data - M.reshape(-1, 1)
    xxsum = (X * np.roll(X, 1, axis=1)).sum(axis=1)
    xxcov = xxsum / (nsamp - 1)
    return xxcov / V


def automatic_channel_mask(data):
    """
    Automatically detect bad channels in the data, returning a binary mask where 'True'
    corresponds to a bad channel. The bad channel detection procedure closely reproduces
    what is being done inside the MeerTRAP real time pipeline.

    Parameters
    ----------
    data : ndarray
        2D array with the data in FT order, with shape (nchan, nsamp)

    Returns
    -------
    mask : ndarray
        Output binary mask
    """
    nchan, __ = data.shape
    S = data.std(axis=1)
    A = spectral_acc1(data)
    radius = max(2, int(0.1 * nchan))
    miqrm, __ = iqrm_mask(S, radius=radius, threshold=3.0)
    # NOTE: Using |acc1| would work better, but currently (Nov 2021)
    # the MeerTRAP pipeline uses acc1 without the absolute value
    # NOTE 2: Using percentiles below 50-th to determine robust stddev
    # would be better, because there are often cases where more than
    # 25% of the channels have excessive |acc1|. But here again we 
    # follow what is currently inside the pipeline.
    macc1 = tukey_mask(A, threshold=2.0)
    return miqrm | macc1


def automatic_clean(data):
    """
    Apply a full RFI cleaning procedure to the data, which closely reproduces
    what is being done inside the MeerTRAP real time pipeline. Returns a
    cleaned copy of the input data and the channel mask that was applied.

    Bad channels are detected then set to zero.
    After that, the data are "spectral whitened" THEN clipped.
    Spectral whitening means normalising individual channels to zero mean and
    unit variance (except of course the automatically masked channels)
    Lastly, the data are clipped to [-3, +3] sigma.

    Parameters
    ----------
    data : ndarray
        Raw data before normalisation, as a 2D array in FT order, i.e. with a
        shape (nchan, nsamp)

    Returns
    -------
    clean_data : ndarray
        Output cleaned data
    mask : ndarray
        Channel mask that was applied, where 'True' corresponds to
        a masked channel
    """
    X = data.astype(np.float32)
    mask = automatic_channel_mask(data)
    X[mask] = 0.0
    Z, __, __ = normalise(X)
    Z = zdot(Z)
    # NOTE: clipping is currently done *after* the application of the zdot filter
    # inside the real-time pipeline
    Z = np.clip(Z, -3, +3)
    return Z, mask

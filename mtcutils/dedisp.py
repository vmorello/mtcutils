import numpy as np


def dispersion_delays(freqs, dm):
    """
    Dispersion delays (seconds) with respect to the first channel
    """
    # The rounded value below is in standard use by pulsar astronomers 
    # (page 129 of Manchester and Taylor 1977)
    kdm = 1.0 / 2.41e-4
    return kdm * dm * (freqs**-2 - freqs[0]**-2)


def dispersion_shifts(freqs, dm, tsamp):
    """
    Dispersion shifts (in number of samples) that correspond to the given DM
    To dedisperse thea data, one must apply the opposite of these shifts
    """
    return np.round(dispersion_delays(freqs, dm) / tsamp).astype(int)


def roll2d(data, shifts):
    """
    Returns a copy of data where the given sequence of circular shifts have 
    been applied to the last axis of data. This can be used to change the DM 
    of a FT data block.
    """
    out = data.copy()
    for ii, sh in enumerate(shifts):
        out[ii] = np.roll(out[ii], sh)
    return out


def dedisperse(data, freqs, dm, tsamp):
    """
    Roll channels of TF data to cancel dedispersion delays.

    Parameters
    ----------
    data: ndarray
        Data in TF order
    freqs: ndarray
        Channel frequencies in MHz
    dm: float
    tsamp: float

    Returns
    -------
    dd: ndarray
        Dedispersed data in TF order
    """
    shifts = dispersion_shifts(freqs, dm, tsamp)
    return roll2d(data, -shifts)


def dedisperse_sum(data, freqs, dm, tsamp):
    """
    Produce the dedispersed time series at given DM

    Parameters
    ----------
    data: ndarray
        Data in TF order
    freqs: ndarray
        Channel frequencies in MHz
    dm: float
    tsamp: float

    Returns
    -------
    dd: ndarray
        One dimensional dedispersed time series at given DM
    """
    shifts = dispersion_shifts(freqs, dm, tsamp)
    out = np.zeros(data.shape[1], dtype='float32')
    for ii, sh in enumerate(shifts):
        out += np.roll(data[ii], -sh)
    return out
try: # python3
    from io import BytesIO
except: # python2
    # There is no BytesIO in python2, since str and bytes are pretty much
    # the same thing in python2
    from StringIO import StringIO as BytesIO

import base64
import time

import numpy as np

from mtcutils import Candidate
from mtcutils.core import scrunch, automatic_clean


def snapshot(fname, width=256, height=128):
    """
    Returns a scrunched version of the candidate data as a uint8 array, where
    the values have been normalized on per-channel basis and scaled to the 
    range 0 - 255 (8-bit depth). This is ready to be written as a lightweight
    greyscale PNG file.

    NOTE: high data values correspond to low values in the 8-bit PNG image 
    output (flipped color scale) so that they appear in black when displayed.

    The data are cleaned from interference before being scrunched and plotted,
    in a way that reproduces the RFI mitigation inside the real-time MeerTRAP
    pipeline (as of late October 2021).

    Parameters
    ----------
    fname : str
        Path the the candidate filterbank
    width : int
        Desired width in pixels, this corresponds to the time axis. The data
        are first cropped (centered) and then time scrunched to match the
        requested width
    height : int
        Desired height in pixels, this corresponds to the freq axis. The data
        are freq scrunched to match the requested height. If 'height' does not
        divide the number of channels in the data, a ValueError is raised.

    Returns
    -------
    snapshot : ndarray
        uint8 ndarray with the data snapshot
    """
    cand = Candidate.from_filterbank(fname)
    X, __ = automatic_clean(cand.data)

    nchan, nsamp = X.shape
    fs = max(1, nchan // height)
    ts = max(1, nsamp // width)

    # Crop (time-centered): we want 'width' samples after scrunching
    offset = (nsamp - ts * width) // 2
    X = X[:, offset:offset+ts*width]

    # and then, scrunch, normalise to conserve the expected noise standard deviation
    X = scrunch(X, f=fs, t=ts) / (ts * fs) ** 0.5
    
    # Clip again, this time for the purposes of getting the color scale right
    # even in edge cases with corrupted data (e.g. mostly zeros or strong RFI)
    vmin = -2.5
    vmax = np.clip(X.max(), 2.5, 6.0)
    X = np.clip(X, vmin, vmax)

    # Scale to 8-bit depth (0 - 255)
    X = (X - vmin) * (255.0 / (vmax - vmin))

    X = 255 - X # flip values so that high values = black in the PNG image
    X = X.astype(np.uint8)
    return X

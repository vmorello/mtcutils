import argparse
import logging
import os

# Set this before importing matplotlib, this works around an annoying thing Qt5 now does
# where it silently doubles the dpi of all figures on high-resolution displays
# See this issue and links therein: https://github.com/matplotlib/matplotlib/issues/12221
os.environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "0"
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rcParams
from tqdm import tqdm

import mtcutils
from mtcutils.dedisp import dedisperse_sum
from mtcutils import parse_maskstring
from mtcutils.core import scrunch
import spyden


log = logging.getLogger('mtcutils.candreport')


def parse_args():
    parser = argparse.ArgumentParser(
        description="Get the best S/N of a MeerTRAP candidate and save diagnostic plots",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument('-v', '--version', action='version', version=mtcutils.__version__)
    parser.add_argument(
        '-m', '--mask', 
        help='Channels to forcibly mask in addition to those masked by IQRM. '
            'This should be a comma-separated list of either channel indices or ranges of indices '
            'given as start:end, where start is INclusive and end EXclusive. '
            'Example: \"10,200:300\" means channel index 10 and indices 200 to 299 inclusive.',
        default=''
    )
    parser.add_argument('--dmlo', type=float, help='Start of DM range to test', default=0.0)
    parser.add_argument('--dmhi', type=float, help='End of DM range to test', default=100.0)
    parser.add_argument('--nbands', type=int, help='In overview plot, scrunch TF data to this number of bands', default=128)
    parser.add_argument('--tscrunch', type=int, help='In overview plot and DDTR plots, scrunch data in time by this factor', default=1)
    parser.add_argument('--no-zdot', dest='nozdot', help='Disable Z-Dot filter', action='store_true')
    parser.add_argument('-o', '--outdir', help='Output directory for plots', default=os.getcwd())
    parser.add_argument('-i', '--interactive', help='Use interactive plotting backend', action='store_true')
    parser.add_argument('fpath', help='Candidate file', type=os.path.realpath)
    return parser.parse_args()


def dedispersion_transform(data, freqs, tsamp, dmlo, dmhi):
    kdm = 1.0 / 2.41e-4
    # Natural DM step, that corresponds to a top to bottom dispersion delay of one sample
    dm_step = tsamp / (kdm * (freqs[-1]**-2 - freqs[0]**-2))

    # Enforce REPRODUCIBLE DM trials. All DM trial values must correspond to an integer multiple
    # of the DM step above
    dmlo = int(np.floor(dmlo / dm_step)) * dm_step
    dmhi = int(np.ceil(dmhi / dm_step)) * dm_step
    log.debug("Using dmlo = {}".format(dmlo))
    log.debug("Using dmhi = {}".format(dmhi))
    dm_trials = np.arange(dmlo, dmhi, dm_step)
    log.debug("dm_step = {}".format(dm_step))

    ddtr = []
    log.info("Calculating brute-force DDTR, DM trials: {}".format(len(dm_trials)))
    for dm in tqdm(dm_trials):
        ddtr.append(dedisperse_sum(data, freqs, dm, tsamp))
    return np.ascontiguousarray(ddtr), dm_trials, dm_step


def main():
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s %(filename)s:%(lineno)-4s %(levelname)-8s %(message)s"
        )
    logging.getLogger('matplotlib').setLevel('WARNING')

    args = parse_args()

    if not args.interactive:
        plt.switch_backend('Agg')

    __, fname = os.path.split(args.fpath)
    basename, __ = os.path.splitext(fname)

    cand = mtcutils.Candidate.from_filterbank(args.fpath)
    cand.normalise()
    mask = cand.apply_chanmask() # calculates and applies both IQRM and ACC1 masks
    log.debug("Channels masked based on stddev (via IQRM) and acc1: {} / {} ({:.2%})".format(
        mask.sum(), cand.nchans, mask.sum() / cand.nchans))

    if args.mask:
        log.info(f"Masking additional channels: {args.mask}")
        mask = mask | parse_maskstring(args.mask, cand.nchans)
        cand.apply_chanmask(mask)
    
    num_masked_chans = mask.sum()
    log.debug("Channels masked in total: {} / {} ({:.2%})".format(
        num_masked_chans, cand.nchans, num_masked_chans / cand.nchans))

    # Z-Dot filter
    zdot_enabled = not args.nozdot
    if zdot_enabled:
        log.debug("Applying Z-Dot filter")
        cand.zdot()
    else:
        log.debug("Skipping Zero-DM filter")

    ### Get robust noise stddev
    data = cand.data
    p25, p75 = np.percentile(data[~mask].ravel(), (25, 75))
    noise_std = (p75 - p25) / 1.3489795
    num_unmasked_chans = (~mask).sum()
    dedisp_noise_std = noise_std * num_unmasked_chans**0.5

    ### Dedisperse
    ddtr, dm_trials, dm_step = dedispersion_transform(
        data, cand.freqs, cand.tsamp, args.dmlo, args.dmhi
    )

    ### SNR
    log.info("Calculating SNR")
    wmin = 1.0
    wmax = 200.0
    gfac = 1.25
    num_widths = int(np.ceil(np.log(wmax/wmin) / np.log(gfac))) + 1
    widths = gfac ** np.arange(num_widths)
    tbank = spyden.TemplateBank.gaussians(widths)
    log.debug("Using Gaussian matched filters with FWHM in geometric progression (factor = {})".format(gfac))
    widths_text = ', '.join(map(lambda x: '{:.2f}'.format(x), widths))
    log.debug("Filter widths (samples) = {}".format(widths_text))
    snr, means, __, models = spyden.snratio(ddtr, tbank, mu='median', sigma=dedisp_noise_std)

    iprof, iwidth, ibin = np.unravel_index(snr.argmax(), snr.shape)
    best_snr = snr[iprof, iwidth, ibin]
    best_dm = dm_trials[iprof]
    best_width = tbank[iwidth].shape_params['w']
    best_width_ms = best_width * cand.tsamp * 1.0e3
    log.debug("Best SNR   = {:.2f}".format(best_snr))
    log.debug("Best DM    = {:.2f}".format(best_dm))
    log.debug("Best Width = {:.2f} samples / {:.3f} ms".format(best_width, best_width_ms))
    log.debug("Centre Bin = {}".format(ibin))

    ### Plots
    times = cand.times
    tscrunch = args.tscrunch
    fscrunch = cand.nchans // args.nbands

    cand.set_dm(best_dm)
    band_freqs = cand.freqs.reshape(-1, fscrunch).mean(axis=1)
    band_foff = band_freqs[1] - band_freqs[0]

    scrunched_tsamp = cand.tsamp * tscrunch
    scrunched_data = (tscrunch * fscrunch) ** -0.5 * scrunch(cand.data, t=tscrunch, f=fscrunch, select='left')
    scrunched_ddtr = tscrunch ** -0.5 * scrunch(ddtr, t=tscrunch, select='left')
    nss = scrunched_data.shape[1]
    scrunched_times = cand.times[::tscrunch][:nss]

    # DDTR
    plt.figure(figsize=(18, 12), dpi=100)
    plt.subplot(311)
    plt.imshow(
        scrunched_ddtr, 
        cmap='Greys', interpolation='nearest', aspect='auto',
        extent=[
            -scrunched_tsamp/2, scrunched_times[-1] + scrunched_tsamp/2, 
            dm_trials[-1] + dm_step/2, dm_trials[0] - dm_step/2
        ]
    )
    plt.ylabel("Trial Dispersion Measure")
    plt.title(
        "File: {}\nZ-Dot = {}, Masked Channels = {}/{} ({:.2%})\nDedispersion Transform".format(
            args.fpath, zdot_enabled, num_masked_chans, cand.nchans, num_masked_chans / cand.nchans))
    ddtr_axes = plt.gca()

    # Dedispersed TF Data
    log.debug("Display TF data with fscrunch = {} in overview plot".format(fscrunch))
    plt.subplot(312, sharex=ddtr_axes)
    ax = plt.gca()
    ax.imshow(
        scrunched_data, 
        cmap='Greys', interpolation='nearest', aspect='auto',
        vmin=-3, vmax=+3,
        extent=[
            -scrunched_tsamp/2, scrunched_times[-1] + scrunched_tsamp/2,
            band_freqs[-1] + abs(band_foff)/2, band_freqs[0] - abs(band_foff)/2
        ]
    )
    ax.set_ylabel("Frequency (MHz)")
    ax2 = ax.twinx()
    ax2.set_ylim(cand.nchans - 0.5, -0.5)
    plt.ylabel("Channel Index")
    plt.title("TF Data at DM = {:.2f} [fscrunch = {}]".format(best_dm, fscrunch))

    # Dedispersed Time Series
    scrunched_dts = scrunched_ddtr[iprof]
    plt.subplot(313, sharex=ddtr_axes)
    plt.plot(scrunched_times, scrunched_dts / dedisp_noise_std, label='Data')

    if tscrunch == 1:  # NOTE: Can't plot best fit for time-scrunched data, would need to re-do the fit
        plt.plot(times, models[iprof] / dedisp_noise_std, label='Best-fit Gaussian')

    plt.xlim(-scrunched_tsamp/2, scrunched_times[-1] + scrunched_tsamp/2)
    plt.ylabel("Normalised Amplitude")
    plt.legend(loc='upper right')
    plt.title(
        "Dedispersed Time Series: DM = {:.2f}, Width = {:.2f} samples | {:.3f} ms, "
        "Peak index = {}, SNR = {:.2f}".format(best_dm, best_width, best_width_ms, ibin, best_snr)
    )
    plt.xlabel("Time Offset (seconds)")
    plt.grid(linestyle=':')

    plt.tight_layout()
    plt.savefig(
        os.path.join(args.outdir, '{}_overview.png'.format(basename))
    )

    plt.figure(figsize=(6, 4), dpi=150)
    plt.plot(dm_trials, snr.max(axis=(1,2)))
    plt.grid(linestyle=':')
    plt.xlim(dm_trials[0], dm_trials[-1])
    plt.xlabel("DM")
    plt.ylabel("Best S/N")
    plt.title("DM Curve")
    plt.tight_layout()
    plt.savefig(
        os.path.join(args.outdir, '{}_dmcurve.png'.format(basename))
    )

    if args.interactive:
        plt.show()


if __name__ == '__main__':
    main()
import os
import glob
import multiprocessing as mp
import logging
import argparse
import json

import numpy as np
from tqdm import tqdm

from mtcutils import Candidate
from mtcutils.reading import SigprocHeader


log = logging.getLogger('mtcutils.collect')


class WrongNumberOfChannels(Exception):
    pass


def get_data(fname, nchans=1024):
    sh = dict(SigprocHeader(fname))
    if sh['nchans'] != nchans:
        raise ValueError("Wrong number of channels ({:d}, expected {:d})".format(sh['nchans'], nchans))

    c = Candidate.from_filterbank(fname)
    bandpass = c.bandpass_std.astype(np.float32)
    metadata = {
        'fname': os.path.realpath(fname),
        'tstart': sh['tstart']
    }
    return metadata, bandpass


class Wrapper(object):
    """ 
    Function-like object that takes a single function argument that we can pass
    to multiprocessing.Pool    
    """
    def __init__(self, **kwargs):
        self._kwargs = kwargs
    
    def __call__(self, fname):
        try:
            return get_data(fname, **self._kwargs)
        except WrongNumberOfChannels:
            return None
        except:
            log.warning("Failed to process {!r}".format(fname))
            return None


DESCRIPTION = """
Collect bandpass standard deviations of a batch of candidates using parallel processes.
Saves two output files:
    bandpasses.npy: a float32 array with shape (ncand, nchans) containing the
        bandpass standard deviations of every candidate.
    bandpasses_metadata.json: a list of dictionaries in JSON format, each containing
        candidate metadata, and in the same order as the lines of bandpasses.npy.
"""


def parse_args():
    def directory(s):
        if not os.path.isdir(s):
            raise ValueError("Argument is not an existing directory")
        return str(s)

    parser = argparse.ArgumentParser(
        description=DESCRIPTION,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        'pattern',
        help='File pattern to match, MUST BE IN DOUBLE QUOTES'
        )
    parser.add_argument(
        '-n', '--nchans',
        help='Select only candidates with this number of channels',
        type=int,
        default=1024)
    parser.add_argument(
        '-o', '--outdir',
        help='Write output files in this directory, defaults to the current directory',
        type=directory,
        default=os.getcwd())
    parser.add_argument(
        '-p', '--processes',
        help='Number of processes to use',
        type=int,
        default=8)
    return parser.parse_args()


def main():
    args = parse_args()

    # Configure logging in main(), since main() is the entry point of the
    # bpfetch console script (see setup.py)
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s %(filename)s:%(lineno)-4s %(levelname)-8s %(message)s"
        )

    files = glob.glob(args.pattern)
    log.info("Found {} files to process matching pattern {}".format(len(files), args.pattern))
    pool = mp.Pool(processes=args.processes)
    
    data = list(tqdm(
        pool.imap_unordered(Wrapper(nchans=args.nchans), files), 
        total=len(files)
        ))
    
    data = [items for items in data if items is not None]
    log.info("Fetched data for {} valid candidates".format(len(data)))

    if data:
        log.info("Writing output to directory: {}".format(os.path.realpath(args.outdir)))
        bandpasses = np.asarray([bp for header, bp in data])
        metadata = [header for header, bp in data]
        np.save(os.path.join(args.outdir, 'bandpasses.npy'), bandpasses)

        with open(os.path.join(args.outdir, 'bandpasses_metadata.json'), 'w') as fobj:
            text = json.dumps(metadata, indent=4)
            fobj.write(text)
        
    else:
        log.info("No candidates fetched, no output will be written")

    log.info("Done")


if __name__ == '__main__':
    main()
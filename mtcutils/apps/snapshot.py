#
# Command-line app to check snapshots of candidate files, as produced by the snapshot() function
# This is for test purposes mostly
#
import argparse
import os
import logging

# Set this before importing matplotlib, this works around an annoying thing Qt5 now does
# where it silently doubles the dpi of all figures on high-resolution displays
# See this issue and links therein: https://github.com/matplotlib/matplotlib/issues/12221
os.environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "0"
import matplotlib.pyplot as plt

import mtcutils


log = logging.getLogger('mtcutils.snapshot')


def parse_args():
    parser = argparse.ArgumentParser(
        description="Display snapshots of candidate files as interactive matplotlib figures",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument('-v', '--version', action='version', version=mtcutils.__version__)
    parser.add_argument('fpath', help='Candidate file(s)', nargs='+')
    return parser.parse_args()


def main():
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s %(filename)s:%(lineno)-4s %(levelname)-8s %(message)s"
        )
    logging.getLogger('matplotlib').setLevel('WARNING')

    args = parse_args()
    num_files = len(args.fpath)

    log.info(f"Files to process: {num_files}")

    for fpath in args.fpath:
        log.info(f"Processing: {fpath!r}")

        try:
            data = mtcutils.snapshot(fpath)
            plt.figure(figsize=(8, 4))
            # NOTE: high data values correspond to low values in the 8-bit PNG image output 
            # (flipped color scale) so that they appear in black when displayed.
            # That's why we use the reversed Greys colormap
            plt.imshow(data, interpolation='nearest', cmap='Greys_r')
            plt.xticks([])
            plt.yticks([])
            plt.title(fpath, font='monospace', fontsize=10)
            plt.tight_layout()
        except Exception as ex:
            log.error(f"Failed to process file {fpath!r}, reason: {ex!s}", exc_info=False)

    plt.show()


if __name__ == '__main__':
    main()
